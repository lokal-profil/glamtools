<?PHP

/*
error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);
ini_set('display_errors', 'On');
*/
include_once ( "../php/common.php" ) ;


$action = get_request ( 'action' , '' ) ;

$out = array ( 'status' => 'OK' , 'data' => array() ) ;

$db = openToolDB ( 'baglama2_p' ) ;

if ( $action == 'overview' ) {

#	$sql = "SET CHARACTER SET utf8" ;
#	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
	$sql = "SELECT groups.id AS id,groups.category AS cat,max(year*100+month) AS date,count(*) AS cnt,sum(total_views) AS sum FROM groups,group_status WHERE groups.id=group_id AND status='VIEW DATA COMPLETE' GROUP BY groups.id ORDER BY groups.category" ;

	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
	while($o = $result->fetch_object()){
		$out['data'][] = array ( 'cat' => utf8_encode ( $o->cat ) , 'gid' => $o->id , 'last' => $o->date , 'count' => $o->cnt , 'sum' => $o->sum ) ;
	}

} else if ( $action == 'monthly' ) {

	$sql = "SET CHARACTER SET utf8" ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
	$gid = get_request ( 'gid' , 0 ) * 1 ;
	$sql = "SELECT * FROM group_status WHERE group_id=$gid ORDER BY year,month" ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
	while($o = $result->fetch_object()){
		$out['data'][$o->year][$o->month] = array ( 'status' => $o->status , 'views' => $o->total_views*1 ) ;
	}

} else if ( $action == 'month_overview' ) {

	$sql = "SET CHARACTER SET utf8" ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
	$gid = get_request ( 'gid' , 0 ) * 1 ;
	$month = get_request ( 'month' , 0 ) * 1 ;
	$year = get_request ( 'year' , 0 ) * 1 ;
	
	$gsid = -1 ;
	$file = '' ;
	$sql = "SELECT * FROM group_status WHERE group_id=$gid AND year=$year AND month=$month" ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
	while($o = $result->fetch_object()){
		$gsid = $o->id ;
		$file = $o->file ;
	}
	
	if ( $file == '' ) { // DB
		$sql = "SELECT DISTINCT server,giu_code AS giu,name,language,project,pages,views FROM sites,gs2site,group_status WHERE site_id=sites.id AND group_status.id=group_status_id AND group_id=$gid AND year=$year and month=$month" ;
		$out['sql'] = $sql ;
		if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
		while($o = $result->fetch_object()){
			$o->pages *= 1 ;
			$o->views *= 1 ;
			$out['data'][] = (array) $o ;
		}
	} else { // File
		$rows = explode ( "\n" , file_get_contents ( $file ) ) ;
		array_shift ( $rows ) ; // Header
		$d = array () ;
		foreach ( $rows AS $row ) {
			if ( trim($row) == '' ) continue ;
			$row = explode ( "\t" , $row ) ;
			$d[$row[0]][0] += $row[3]*1 ; // VIews
			$d[$row[0]][1] ++ ; // Pages
		}
		
		$sites = array() ;
		$sql = "SELECT * FROM sites" ;
		if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
		while($o = $result->fetch_object()){
			$sites[$o->giu_code] = $o ;
		}
		
		foreach ( $d AS $giu => $views ) {
			$out['data'][] = array (
				'server' => $sites[$giu]->server ,
				'giu' => $giu ,
				'name' => $sites[$giu]->name ,
				'language' => $sites[$giu]->language ,
				'project' => $sites[$giu]->project ,
				'pages' => $views[1] ,
				'views' => $views[0]
			) ;
		}
	}

	function cmp ( $a , $b ) {
		if ( $a['views'] == $b['views'] ) return 0 ;
		if ( $a['views'] < $b['views'] ) return 1 ;
		return -1 ;
	}
	
	usort($out['data'], "cmp");


	$format = get_request ( 'format' , 'json' ) ;
	if ( $format == 'tabbed' ) {
//		header('Content-type: text/plain');
		header('Content-type: application/octet-stream'); // text/plain
		header("Content-disposition: attachment; filename=baglama.monthly_overview.$gid.$year-$month.tab");
		print "Site\tPages\tViews\n" ;
		foreach ( $out['data'] AS $v ) {
			if ( $v['name'] == '' ) print $v['language'] . "." . $v['project'] ;
			else print $v['name'] . " " . ucfirst ( $v['project'] ) ;
			print "\t" . $v['pages'] . "\t" . $v['views'] . "\n" ;
		}
		exit ( 0 ) ;
	}

} else if ( $action == 'month_site' ) {

#	$sql = "SET CHARACTER SET utf8" ;
#	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
	$gid = get_request ( 'gid' , 0 ) * 1 ;
	$month = get_request ( 'month' , 0 ) * 1 ;
	$year = get_request ( 'year' , 0 ) * 1 ;
	$giu = get_request ( 'giu' , 0 ) ;
	$max = get_request ( 'max' , 0 ) * 1 ;

	$gsid = -1 ;
	$file = '' ;
	$sql = "SELECT * FROM group_status WHERE group_id=$gid AND year=$year AND month=$month" ;
	if(!$result = $db->query($sql)) die('There was an error running the query 1 [' . $db->error . ']');
	while($o = $result->fetch_object()){
		$gsid = $o->id ;
		$file = $o->file ;
	}
	
	if ( $file == '' ) { // DB
		$site = 0 ;
		$sql = "SELECT * FROM sites where giu_code='" . $db->real_escape_string ( $giu ) . "'" ;
		if(!$result = $db->query($sql)) die('There was an error running the query 2 [' . $db->error . ']');
		while($o = $result->fetch_object()){
			$site = $o->id ;
		}
		
		$sql = "SELECT title,namespace_id,views,group_concat(DISTINCT image separator '|') AS files from views,group2view WHERE group2view.view_id=views.id AND site=$site AND done=1 AND group_status_id=$gsid GROUP BY view_id ORDER BY views DESC" ;
		if ( $max > 0 ) $sql .= " LIMIT $max" ;
		if(!$result = $db->query($sql)) die('There was an error running the query 3 [' . $db->error . ']');
		while($o = $result->fetch_object()){
			$o->views *= 1 ;
			$o->files = explode ( '|' , $o->files ) ;
			$out['data'][] = (array) $o ;
		}
	} else { // File

		$rows = explode ( "\n" , file_get_contents ( $file ) ) ;
		array_shift ( $rows ) ; // Header
		foreach ( $rows AS $row ) {
			if ( trim ( $row ) == '' ) continue ;
			$row = explode ( "\t" , $row ) ;
			if ( $row[0] != $giu ) continue ;
			$o = array (
				'title' => (str_replace('_',' ',urldecode($row[1]))) ,
				'namespace_id' => 0 ,
				'views' => $row[3]*1 ,
				'files' => explode ( '|' , ($row[2]) )
			) ;
			$out['data'][] = $o ;
		}
		
		function cmp ( $a , $b ) {
			if ( $a['views'] == $b['views'] ) return 0 ;
			if ( $a['views'] < $b['views'] ) return 1 ;
			return -1 ;
		}
		
		usort($out['data'], "cmp");
		if ( $max > 0 ) {
			$out['data'] = array_slice ( $out['data'] , 0 , 100 ) ;
		}

	}
	
	$format = get_request ( 'format' , 'json' ) ;
	if ( $format == 'tabbed' ) {
//		header('Content-type: text/plain');
		header('Content-type: application/octet-stream'); // text/plain
		header("Content-disposition: attachment; filename=baglama.month_site.$gid.$year-$month.$giu.tab");
		print "Title\tViews\tFile(s)\n" ;
		foreach ( $out['data'] AS $v ) {
			print $v['title'] . "\t" . $v['views'] . "\t" . implode('|',$v['files']) . "\n" ;
		}
		exit ( 0 ) ;
	}
	
}

/*
if ( isset($_REQUEST['test']) ) {
	$x = json_encode ( $out ) ;
	header('Content-type: text/html');
	print "<pre>" ;
	print var_dump($x) ;
	exit ( 0 ) ;
}
*/

//print_r ( $out ) ; exit(0);

header('Content-type: application/json');
print json_encode ( $out ) ;
//if ( isset ( $_REQUEST['testing'] ) ) { print get_common_header() ."$sql" ; exit ( 0 ) ; }

exit ( 0 ) ;

?>
