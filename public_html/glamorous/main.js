var month_range ;
var est_files_in_cat_tree ;
var page_view_month ;
var auto_thumbs = true ;
var auto_show_page_views = false ;
var file_counter ;
var pageids ;
var files ;
var gu_running = 0 ;
var wiki2files ;
var namespace0 = false ;
var global_stats ;
var show_pageviews = false ;
var last_ucts = '' ;
var last_ucts_v = '' ;
var page_views_stats = { finished : false } ;
var daily_views_plot ;


function showExample ( k , text , depth ) {
	$("input[name='mode']")[k].checked = true ;
	$('#'+$("input[name='mode']:checked").val()).val ( text ) ;
	if ( undefined !== depth ) {
		if ( depth == $('#depth').attr('placeholder') ) $('#depth').val('');
		else $('#depth').val(depth);
	}
	runTool();
}

function getGlobalUsageNoIDs () {
	var left = files.length ;
	var new_files = {} ;
	$.each ( files , function ( k , v ) {
		v.checkExists ( function ( o ) {
			new_files[o.pageid] = o ;
			left-- ;
			if ( left > 0 ) return ;
			files = new_files ;
			getGlobalUsage () ;
		} , function ( o ) {
			left-- ;
			file_counter-- ;
			if ( left > 0 ) return ;
			files = new_files ;
			getGlobalUsage () ;
		} ) ;
	} ) ;
}

function getGlobalUsage () {
	wiki2files = {} ;
	pageids = [ [] ] ;
	var pidc = 0 ;
	$.each ( files , function ( k , v ) {
		files[k].globalusage = [] ;
		if ( pageids[pidc].length >= 50 ) {
			pidc++ ;
			pageids.push ( [] ) ;
		}
		pageids[pidc].push ( k ) ;
	} ) ;
	
	gu_running = pageids.length ;

	// Get global usage
	$.each ( pageids , function ( k , v ) {
		pageids[k] = v.join('|') ;
		getGlobalUsagePackage ( pageids[k] , function () {
			gu_running-- ;
			if ( gu_running == 0 ) {
				
				var pagecount = 0 ;
				$.each ( files , function ( file_id , file ) { pagecount += object_length(file.globalusage) } ) ;
				month_range.basetime = pagecount * ( 1/50 ) ;
				updateTimeEst() ;
				wikiDataCache.ensureSiteInfo ( [ { lang:'en' , project:'wikipedia' } ] , updateResultDisplay ) ;
			}
		} ) ;
	} ) ;
}

function updateTimeEst () {
	var range = month_range.getRange() ;
	var t = Math.floor ( month_range.basetime * range.length ) ;
	$('#'+month_range.id_button).html ( 'Show page views (ca. ' + prettyTime ( t ) + ')' ) ;
}

function updateResultDisplay () {
	$('#loading_status').remove() ;
	$('#gu_results').html('') ;
	$('#gu_details').html('') ;

	if ( namespace0 ) $('#namespace0').prop("checked", true );
	else $('#namespace0').prop("checked", false );
	
	if ( page_views_stats.finished ) {
		page_views_stats = { finished : false } ;
		showPageViews() ;
		return ;
	}
	showGUresults () ;
}

function updatePermalink () {
	namespace0 = $('#namespace0').prop('checked') ;
	var mode = $("input[name='mode']:checked").val() ;
	var text = $('#'+mode).val() ;
	
	var url = "?mode=" + mode + "&text=" + encodeURIComponent ( text ) ;
	if ( $('#depth').val() != '' ) url += "&depth=" + encodeURIComponent ( $('#depth').val() ) ;
	if ( namespace0 ) url += "&ns0=1" ;
	if ( show_pageviews ) url += "&pageviews=1&month=" + encodeURIComponent ( page_view_month ) ;
	
	var h = "<a>Permalink</a> to this query" ;
	$('#permalink').html ( h ) ;
	$('#permalink a').attr ( 'href' , url ) ;
}

function showGUresults () {
	updatePermalink () ;
	namespace0 = $('#namespace0').prop('checked') ;
	var mode = $("input[name='mode']:checked").val() ;
	var text = $('#'+mode).val() ;
	
	if ( getBlobBuilder() ) {
		$('#download_row').show() ;
		var dh = '' ;
		dh += "<button class='btn btn-mini' onclick='downloadData(\"xml\");return false'>Download as XML</button>" ;
		$('#download').html ( dh ) ;
	}
	
	var h = '' ;
	h += "<div id='maintable_pager' class='pager'></div>" ;
	h += "<table id='maintable' class='table table-condensed table-striped tablesorter'>" ;
	h += "<thead><tr><th>Wiki</th><th>Distinct files used</th><th>Pages using files</th><th>Total file usages</th>" ;
	if ( show_pageviews ) h += "<th>Page views for " + page_view_month.replace('|',' &ndash; ') + "</th>" ;
	h += "</tr></thead><tbody>" ;
	
	var wikis = [] ;
	$.each ( wiki2files , function ( k , v ) { wikis.push ( k ) } ) ;
	wikis = wikis.sort() ;
	
	var t_pui = 0 ; // Pages using image(s)
	var t_iu = 0 ; // Total images uses
	var distinct_images_used = {} ; // Distinct images used
	
	global_stats = { wikis : {} } ;
	
	$.each ( wikis , function ( dummy , wiki ) {
		var iu = 0 ;
		var images_used = {} ;
		var pages_using = {} ;
		$.each ( wiki2files[wiki] , function ( k , v ) {
			if ( namespace0 && v[3] != 0 ) return ;
			iu++ ;
			distinct_images_used[v[2]]++ ;
			images_used[v[2]]++ ;
			pages_using[v[1]]++ ;
		} ) ;
		
		var diu = 0 ;
		$.each ( images_used , function ( k , v ) { diu++ } ) ;

		var pui = 0 ;
		$.each ( pages_using , function ( k , v ) { pui++ } ) ;
		
		t_pui += pui ;
		t_iu += iu ;
		
		global_stats.wikis[wiki] = { distinct_files : diu , pages_using : pui , total_usage : iu } ;
		
		h += "<tr><td><b>" + wiki + "</b></td>" ;
		h += "<td class='numcell'>" + prettyNumber(diu) + "</td>" ;
		h += "<td class='numcell'>" + prettyNumber(pui) + "</td><td class='numcell'>" + prettyNumber(iu) + "</td>" ;
		if ( show_pageviews ) h += "<td class='numcell' id='wikipageviews_" + sanitizeID(wiki) + "'>" + prettyNumber(page_views_stats.finished?page_views_stats.wikipageviews[wiki]:0) + "</td>" ;
		h += "</tr>" ;
	} ) ;

	var t_diu = 0 ;
	$.each ( distinct_images_used , function ( k , v ) { t_diu++ } ) ;
	
	h += "</tbody><tfoot><tr><td><b>Total</b></td><td class='numcell'>" ;
	h += prettyNumber(t_diu) + "<br/>" ;
	
	var pdiu = Math.floor ( 1000 * t_diu / file_counter ) / 10 ;
	h += "(" + pdiu + "% of all files)";
	
	global_stats.distinct_files = t_diu ;
	global_stats.total_usage = t_iu ;
	global_stats.pages_using = t_pui ;
	
	h += "</td><td class='numcell'>" + prettyNumber(t_pui) + "</td><td class='numcell'>" + prettyNumber(t_iu) + "</td>" ;
	if ( show_pageviews ) h += "<td class='numcell' id='wikipageviews_total'>" + prettyNumber(page_views_stats.finished?page_views_stats.wikipageviews['total']:0) + "</td>" ;
	h += "</tr></tfoot>" ;
	h += "</table>" ;

	$('#status_row').remove() ;
//	$('#gu_results').remove() ;
	$('#gu_details').html('') ;
	$('#gu_results').html ( h ) ;
	$('#maintable_pager').load ( '../resources/html/pager.html' , function () {
		$('#maintable').tablesorter({ sortList: [[1,1]] , textExtraction : tableSorterExtractPrettyNumbers }).tablesorterPager({container: $("#maintable_pager")});
		$('#maintable_pager').css ( { position:'' , top:'' } ) ;
		showGUdetails () ;
	} ) ;
}

function showGUdetails () {
	var file_ids = [] ;
	$.each ( files , function ( id , data ) { file_ids.push ( id ) } ) ;
	file_ids = file_ids.sort ( function ( a , b ) {
		return ( files[b].globalusage.length - files[a].globalusage.length ) ;
	} ) ;
	
	var top = 2000 ;
	
	var h = '' ;
	h += "<div id='gu_details_table_pager' class='pager'></div>" ;
	h += "<table id='gu_details_table' class='table table-condensed table-striped tablesorter'>" ;
	h += "<thead><tr><th>File</th><th style='padding-right:20px'>Uses</th>" ;
	if ( show_pageviews ) h += "<th  style='padding-right:20px' nowrap>Page views</th>" ;
	h += "<th>Details</th></tr></thead><tbody>" ;
	
	var cnt = 0 ;
	var use_file_ids = [] ;
	$.each ( file_ids , function ( dummy , file_id ) {
		var data = files[file_id] ;
//		if ( data.globalusage.length == 0 ) return false ; // No usage, no joy

		var gul = 0 ;
		if ( namespace0 ) {
			$.each ( data.globalusage , function ( k , v ) {
				if ( v.ns == 0 ) gul++ ;
			} ) ;
		} else gul = data.globalusage.length ;

		h += "<tr>" ;
		h += "<td style='width:100%'>" ;
		h += "<div>" + data.getLink ( { target:'_blank' , no_namespace:true } ) + "</div>" ;
		h += "<div class='thumb_wrapper' fileid='" + file_id + "' id='img" + file_id + "' style='display:none'></div>" ;
		h += "</td>" ;
		h += "<td class='numcell'>" + prettyNumber ( gul ) + "</td>" ;
		if ( show_pageviews ) {
			h += "<td class='numcell' id='filepageviews_" + sanitizeID(file_id) + "'>" ;
			h += prettyNumber(page_views_stats.finished?page_views_stats.filepageviews[file_id]:0)
			h += "</th>" ;
		}
		h += "<td>" ;
		if ( data.globalusage.length != 0 ) {
			h += "<div style='text-align:right'>" ;
			h += "<button id='b_gu_show_" + file_id + "' class='btn btn-mini gu_show' onclick='gu_details(true," + file_id + ");return false'>Show&nbsp;details</button>" ;
//			h += "<button id='b_gu_hide_" + file_id + "' class='btn btn-mini gu_hide' onclick='gu_details(false," + file_id + ");return false' style='display:none'>Hide&nbsp;details</button>" ;
			h += "</div>" ;
			h += "<div id='gu_details_" + file_id + "' style='display:none;max-height:300px;overflow:auto;padding-right:20px'></div>" ;
			use_file_ids.push ( file_id ) ;
		}
		h += "</td>" ;
		h += "</tr>" ;
		cnt++ ;
		if ( cnt >= top ) return false ;
	} ) ;
	
	h += "</tbody></table>" ; 
//	h += "</div>" ;

//	h = "<div id='gu_details'><h3>Details for " + prettyNumber ( cnt ) + " files</h3>" + h ;
//	h = "<div>Details for " + prettyNumber ( cnt ) + " files</div>" + h ;

//	$('#gu_details').remove() ;
	$("#gu_details").html ( h ) ;

	startGUpager () ;
	
	if ( auto_show_page_views ) {
		auto_show_page_views = false ;
		month_range.buttonCallback ( month_range ) ;
	}

}

function startGUpager () {
	$('#gu_details_table_pager').load ( '../resources/html/pager.html' , function () {
		$('#gu_details_table').tablesorter( { 
			sortList: [[1,1]] , 
			textExtraction : tableSorterExtractPrettyNumbers
		}).tablesorterPager( { 
			container: $("#gu_details_table_pager") ,
			rtCallback : guPagerCallback
		});
		$('#gu_details_table_pager').css ( { position:'' , top:'' } ) ;
	} ) ;
}

function guPagerCallback ( tbody ) {
	$(tbody).find('.thumb_wrapper').each ( function () {
		var file_id = $(this).attr('fileid') ;
		var data = files[file_id] ;
		data.getImageInfo ( { callback : function ( o ) {
			var h = "<img border=0 width='"+o.imageinfo.thumbwidth+"'/>" ;
			$('#img'+file_id).html(h).show() ;
			$('#img'+file_id+' img').attr ( 'src' , o.imageinfo.thumburl ) ;
			$('.gu_show').click() ;
		} } ) ;
	} ) ;
}

function gu_details ( do_show , file_id ) {
	if ( do_show ) {
		if ( !auto_thumbs ) {
			files[file_id].getImageInfo ( { callback : function ( o ) {
				var h = "<img border=0 width='"+o.imageinfo.thumbwidth+"'/>" ;
				$('#img'+file_id).html(h).show() ;
				$('#img'+file_id+' img').attr ( 'src' , o.imageinfo.thumburl ) ;
			} } ) ;
		}
		
		// Pre-cache language.projects
		var lp = {} ;
		$.each ( files[file_id].globalusage , function ( k , o ) {
			var m = o.wiki.match ( /^(.+)\.([^.]+)\.org$/ ) ;
			o.lang = m[1] ;
			o.project = m[2] ;
			if ( o.ns >= 100 ) lp[o.lang+'|'+o.project] = { lang:o.lang , project:o.project } ; // ns < 100 has en.wikipedia fallback
		} ) ;
		var lp2 = [] ;
		$.each ( lp , function ( k , v ) { lp2.push ( v ) } ) ;

		$('#b_gu_show_'+file_id).hide() ;
		$('#b_gu_hide_'+file_id).show() ;
		$('#gu_details_'+file_id).html('<i>Loading&nbsp;namespace&nbsp;data...</i>').show() ;
		
		wikiDataCache.ensureSiteInfo ( lp2 , function () {
			var uses = {} ;
			$.each ( files[file_id].globalusage , function ( k , o ) {
				var v = new WikiPage ( o ) ;
				if ( namespace0 && o.ns != 0 ) return ;
				if ( v.ns != 0 ) v.title = v.title.replace ( /^[^:]+:/ , '' ) ;
				var p = v.lang + "." + v.project ;
				if ( undefined === uses[p] ) uses[p] = [] ;
				uses[p].push ( v.getLink ( { target:'_blank' } ) ) ;
			} ) ;
	
	
			var h = "<table border=0 cellspacing=0 cellpadding=2 style='font-size:8pt'>" ;
			$.each ( uses , function ( project , v ) {
				h += "<tr>" ;
				h += "<td>" + project + "</td>" ;
				h += "<td nowrap>" ;
				h += v.join ( '<br/>' ) ;
				h += "</td>" ;
				h += "</tr>" ;
			} ) ;
			h += "</table>" ;
			
			$('#gu_details_'+file_id).html(h) ;
		} ) ;
		
	} else {
		if ( !auto_thumbs ) $('#img'+file_id).html ( '' ) ;
		$('#b_gu_show_'+file_id).show() ;
		$('#b_gu_hide_'+file_id).hide() ;
		$('#gu_details_'+file_id).html('').hide() ;
	}
}

function getGlobalUsagePackage ( pids , callback , d ) {
	if ( pids == '' ) return callback ( pids ) ;

	var params = {
		action : 'query' ,
		prop : 'globalusage' ,
		pageids : pids ,
		guprop : 'pageid|namespace' ,
		gulimit : 500 ,
		format : 'json',
		rawcontinue : 1
	} ;
	
	if ( undefined !== d ) {
		$.each ( d.query.pages , function ( pid , v ) {
			$.each ( v.globalusage , function ( k2 , v2 ) {
				// FIXME namespace filter
				files[pid].globalusage.push ( v2 ) ;
				if ( undefined === wiki2files[v2.wiki] ) wiki2files[v2.wiki] = [] ;
				wiki2files[v2.wiki].push ( [ v2.title , v2.pageid , pid , v2.ns ] ) ; // pid is the ID of the file!
			} ) ;
		} ) ;
		if ( undefined === d['query-continue'] || undefined === d['query-continue'].globalusage ) return callback ( pids ) ;
		params.gucontinue = d['query-continue'].globalusage.gucontinue ;
	}
	
	var url = '//commons.wikimedia.org/w/api.php' ;
	$.getJSON ( url + "?callback=?" , params , function ( data ) {
		getGlobalUsagePackage ( pids , callback , data ) ;
	} ) ;
	
}

function getPagesInPagePile( id, callback ) {
	var url = '//tools.wmflabs.org/pagepile/api.php' ;
	var params = {
		format : 'json' ,
		action : 'get_data' ,
		id : id ,
		doit : 1
	} ;
	$.getJSON ( url + "?callback=?" , params , function ( data ) {
		callback ( data ) ;
	} ) ;
}

function initMonthRange () {
	month_range = new MonthRange ( {
		root_id : 'td_pageviews' ,
		buttonCallback : function () { showPageViews() } ,
		dateChangeCallback : function ( o ) {
			if ( (page_view_month||'') == o.getKey() ) {
				$('#'+o.id_button).attr('disabled','disabled');
			} else {
				$('#'+o.id_button).removeAttr('disabled');
			}
			updateTimeEst () ;
		}
	} ) ;
	
	if ( auto_show_page_views ) {
		month_range.setKey ( page_view_month ) ;
	}
}

function initTabs () {
	$('#load_progress_container').remove();
	var h = "" ;
	
	h += '<ul class="nav nav-tabs" id="resultsTab">' ;
	h += '<li><a href="#div_toptable" data-toggle="tab">Process</a></li>' ;
	h += '<li><a href="#gu_results" data-toggle="tab">Global file usage</a></li>' ;
	h += '<li><a href="#gu_details" data-toggle="tab">File usage details</a></li>' ;
	h += '<li><a href="#daily_views_container" data-toggle="tab">Daily views</a></li>' ;
	h += '</ul>' ;
	
	h += '<div class="tab-content">' ;
	h += "<div class='tab-pane' id='div_toptable'></div>" ;
	h += "<div class='tab-pane' id='gu_results'></div>" ;
	h += "<div class='tab-pane' id='gu_details'></div>" ;
	h += "<div class='tab-pane' id='daily_views_container'><i>Requires loading page view data in the \"Process\" tab.</i></div>" ;
	h += '</div>' ;

	$('#results').html ( h ) ;
	$('#resultsTab a:first').tab ('show') ;
//div_toptable
}

function runTool () {
	var mode = $("input[name='mode']:checked").val() ;
	var text = $('#'+mode).val() ;
	
	var namespaces = undefined ;

	page_views_stats = { finished : false } ;
	show_pageviews = false ;
	file_counter = undefined ;
	pageids = undefined ;
	gu_running = 0 ;
	wiki2files = undefined ;
	
	files = {} ;
	
	$('#result_container').show() ;
	$('#results').html ( "<span id='loading_status' class='label label-info'>Loading file list...</span>" ) ;
	
	if ( mode == 'category' ) {
		$('#results').append ( '<div id="load_progress_container" style="display:none" class="progress"><div id="load_progress" class="bar" style="width:0%;"></div></div>' ) ;
		var sourcecat = new WikiPage ( { ns:14 , lang:'commons' , title:text } ) ;
		$('#category').keyup() ;
		var depth = $('#depth').val() ;
		if ( depth == '' ) depth = $('#depth').attr('placeholder') ;

		var p = new WikiPage ( { title : text , lang : 'commons' , project : 'wikimedia' , ns : 14 } ) ;
		p.getPagesInCategoryTree ( { depth : depth , namespaces : [ 6 ] , cacheable : true , callback : function ( d ) {
			initTabs() ;
			file_counter = 0 ;
			$.each ( d , function ( k , v ) { if ( v.ns == 6 ) file_counter++ } ) ;
			
			$('#div_toptable').load ( 'toptable.html' , function () {
				initMonthRange() ;
				$('#fic').html ( prettyNumber ( file_counter ) + ' files in category "' + sourcecat.getLink({target:'_blank',no_namespace:true}) + '"' ) ;
				$('#status').html ( "Loading global usage data..." ) ;
				$('#namespace0').change ( function () {
					namespace0 = $('#namespace0').prop('checked') ;
					updateResultDisplay();
				} ) ;
				files = d ;
				getGlobalUsage () ;
			} ) ;
		} , status : function ( d ) {
			if ( undefined !== est_files_in_cat_tree ) {
				$('#load_progress_container').show() ;
				$('#load_progress').width ( ( Math.floor ( 1000 * d.data.page_counter / est_files_in_cat_tree ) / 10 ) + '%' ) ;
			}
			$('#loading_status').html ( "Loading; " + prettyNumber ( d.data.page_counter ) + " files loaded" ) ;
		} } ) ;
		
	} else if ( mode == 'user' ) {
		initTabs() ;
		$('#user').keyup() ;
		var p = new WikiPage ( { title : text , ns : 2 , lang : 'commons' } ) ;

		p.getFilesUploadedByUser ( function ( o ) {
			var i = [] ;
			$.each ( o.logevents , function ( k , v ) {
				i.push ( new WikiPage ( { title : v.replace(/^[^:]+:/,'') , ns : 6 , lang : 'commons' } ) ) ;
			} ) ;
			file_counter = i.length ;
			files = i ;
			$('#div_toptable').load ( 'toptable.html' , function () {
				initMonthRange() ;
				$('#fic').html ( prettyNumber ( file_counter ) + ' files by user "' + p.getLink({target:'_blank',no_namespace:true}) + '"' ) ;
				$('#status').html ( "Loading global usage data..." ) ;
				$('#namespace0').change ( function () {
					namespace0 = $('#namespace0').prop('checked') ;
					updateResultDisplay();
				} ) ;
				getGlobalUsageNoIDs () ;
			} ) ;
		} ) ;

	} else if ( mode == 'pagepile' ) {
		initTabs() ;
		$('#pagepile').keyup() ;

		getPagesInPagePile( text, function ( data ) {
			var i = [] ;
			if ( data.wiki === 'commonswiki' ) {
				$.each ( data.pages , function ( k , v ) {
					if ( v.indexOf("File:") === 0 ) {
						i.push ( new WikiPage ( { title : v.replace(/^[^:]+:/,'') , ns : 6 , lang : 'commons' } ) ) ;
					}
				} ) ;
			}
			file_counter = i.length ;
			files = i ;
			$('#div_toptable').load ( 'toptable.html' , function () {
				initMonthRange() ;
				$('#fic').html ( prettyNumber ( file_counter ) + ' files in pagepile "' + text + '"' ) ;
				$('#status').html ( "Loading global usage data..." ) ;
				$('#namespace0').change ( function () {
					namespace0 = $('#namespace0').prop('checked') ;
					updateResultDisplay();
				} ) ;
				getGlobalUsageNoIDs () ;
			} ) ;
		} ) ;

	} else if ( mode == 'page' ) {
		initTabs() ;
	
		$('#page').keyup() ;
		var pages = text.split ( '|' ) ;
		var pages_running = pages.length ;
		$.each ( pages , function ( dummy , page ) {
			var p = new WikiPage ( { title : page , ns : 0 , lang : 'commons' } ) ;
			p.parseNamespaceFromTitle ( function () {
				p.getImages ( function ( o ) {
					var i = [] ;
					$.each ( o.images , function ( k , v ) {
						i.push ( new WikiPage ( { title : v.replace(/^[^:]+:/,'') , ns : 6 , lang : 'commons' } ) ) ;
					} ) ;
					file_counter = i.length ;
					files = i ;
//					console.log ( files ) ;

					$('#div_toptable').load ( 'toptable.html' , function () {
						initMonthRange() ;
						$('#fic').html ( prettyNumber ( file_counter ) + ' files in page "' + p.getLink({target:'_blank'}) + '"' ) ;
						$('#status').html ( "Loading global usage data..." ) ;
						$('#namespace0').change ( updateResultDisplay ) ;
//						files = d ;
						getGlobalUsageNoIDs () ;
//						getGlobalUsage () ;
					} ) ;


				} ) ;
			} ) ;
		} ) ;

	}
}

function initializeFromParameters () {
	var params = getUrlVars() ;
	if ( undefined === params.mode || undefined === params.text ) return ;
	if ( undefined !== params.depth ) $("#depth").val ( parseInt ( params.depth ) ) ;
	if ( undefined !== params.ns0 ) namespace0 = true ;
	if ( undefined !== params.pageviews && undefined !== params.month ) {
		show_pageviews = true ;
		page_view_month = params.month ;
		auto_show_page_views = true ;
	}
	$('#'+params.mode).val ( decodeURIComponent ( params.text ) ) . change() ;
	runTool() ;
}

function updateCatTreeStats ( show ) {
	est_files_in_cat_tree = undefined ;
	var title = $('#category').val() ;
	var depth = $('#depth').val() || $('#depth').attr('placeholder') ;
	var ucts = title + "|" + depth ;
	if ( $('#fs_cat').hasClass('success') ) {
		if ( ucts != last_ucts ) {
			last_ucts = ucts ;
			$('#cat_tree').html("<i>Determining category tree size...</i>").show() ;
			var p = new WikiPage ( { title : title , ns : 14 , lang : 'commons' } ) ;
			p.getCategoryInfo ( { depth : depth , callback : function ( d ) {
				last_ucts_v = "&le;" + prettyNumber ( d.files ) + " files in " + prettyNumber ( d.catcount ) + " categories." ;
				last_ucts_v += " Estimated load time: " + prettyTime ( d.files / 1500 ) + ', plus global usage data' ;
				$('#cat_tree').html ( last_ucts_v ) ;
				est_files_in_cat_tree = d.files ;
			} } ) ;
		} else {
			$('#cat_tree').html ( last_ucts_v ).show() ;
		}
	} else {
		$('#cat_tree').hide() ;
	}
}

function downloadData ( format ) {
	var currentTime = new Date() ;
	var month = currentTime.getMonth() + 1 ;
	var day = currentTime.getDate() ;
	var year = currentTime.getFullYear() ;
	
	var blob = getBlobBuilder() ;
	var mode = $("input[name='mode']:checked").val() ;
	var target = $('#'+mode).val() ;
	var filename = "GLAMorous." + target + '.' + year + '-' + month + '-' + day + ( namespace0 ? '.articles' : '' ) ;
	var filetype ;

	
	if ( format == 'xml' ) {
		filename += '.xml' ;
		filetype = "text/xml;charset=utf-8" ;
		var t = '<?xml version="1.0"?>' ;
		
		var permalink = window.location.origin + window.location.pathname + $('#permalink a').attr('href') ;
		
		t += '<results ' + mode + '="' + escattr(target) + '" images_in_category="' + file_counter + '" query_url="' + escattr(permalink) + '">' ;

 		t += '<stats total_usage="' + global_stats.total_usage + '" distinct_images="' + global_stats.distinct_files + '" pages_using_files="' + global_stats.pages_using + ' ">' ;
 		$.each ( global_stats.wikis , function ( wiki , data ) {
 			t += '<usage project="' + wiki + '" usage_count="' + data.total_usage + '" distinct_images="' + data.distinct_files + '" pages_using_files="' + data.pages_using + '"/>' ;
 		} ) ;
		t += '</stats>' ;
		
		t += '<details>' ;
		
		$.each ( files , function ( file_id , file ) {
			var len = object_length(file.globalusage||{}) ;
			if ( len == 0 ) return ; // No need to output
			var by_project = {} ;
			$.each ( (file.globalusage||{}) , function ( dummy , page ) {
				if ( undefined === by_project[page.wiki] ) by_project[page.wiki] = {} ;
				var title = { ns:'' , title:page.title } ;
				var m = page.title.match ( /^([^:]+):(.+)$/ ) ;
				if ( m != null ) {
					if ( namespace0 ) return ;
					title = { ns:m[1] , title:m[2] } ;
				}
				if ( undefined === by_project[page.wiki][title.ns] ) by_project[page.wiki][title.ns] = [] ;
				by_project[page.wiki][title.ns].push ( title.title ) ;
			} ) ;
			t += '<image name="' + escattr(file.getNiceTitle()) + '" url_page="https:' + escattr(file.getNiceURL()) + '" usage="' + len + '">\n' ; // TODO url_thumbnail
			$.each ( by_project , function ( wiki , namespaces ) {
				t += '<project name="' + escattr(wiki) + '">\n' ;
				$.each ( namespaces , function ( ns , pages ) {
					t += '<namespace ns="' + escattr(ns) + '">\n' ;
					$.each ( pages , function ( dummy , page ) {
						t += '<page title="' + escattr(page.replace(/_/g,' ')) + '"/>\n' ;
					} ) ;
					t += '</namespace>' ;
				} ) ;
				t += '</project>\n' ;
			} ) ;
			t += '</image>\n' ;
		} ) ;
		
		t += '</details>' ;
		
		t += "</results>" ;

		blob.append ( t ) ;
	} else return ; // BAAAAD format!

	saveAs(blob.getBlob(filetype), filename);
}

function showPageViews () {
	updatePermalink () ;
	$('#'+month_range.id_button).attr('disabled','disabled');
	$('#td_pageviews').append('<div id="run_process_container" style="margin-top:5px" class="progress"><div id="run_progress" class="bar" style="width:0%;"></div></div>') ;
	show_pageviews = true ;
	page_view_month = month_range ? month_range.getKey() : '' ;
	page_views_stats = { finished : false , total : 0 , done : 0 , wikipageviews : { total : 0 } , wiki_daily_pageviews : {} , filepageviews : {} } ;
	updateResultDisplay() ;
	

	var range = month_range.getRange() ;
	$.each ( files , function ( file_id , file ) {
		page_views_stats.filepageviews[file_id] = 0 ;
		$.each ( (file.globalusage||{}) , function ( dummy , page ) {
			page_views_stats.wikipageviews[page.wiki] = 0 ;
			page_views_stats.total += range.length ;
		} ) ;
	} ) ;
	page_views_stats.done = 0 ;
	
	$.each ( range , function ( dummy1 , the_month ) {
		$.each ( files , function ( file_id , file ) {
			$.each ( (file.globalusage||{}) , function ( dummy , page ) {
				if ( namespace0 && page.ns != 0 ) {
					page_views_stats.done++ ;
					return ;
				}
				var m = page.wiki.match ( /^(.+)\.([^.]+).org$/ ) ;
				if ( m == null ) return ; // Paranoia
				var wp = new WikiPage ( page ) ;
				wp.lang = m[1] ;
				wp.project = m[2] ;
				wp.getViewStats ( { date : the_month.replace(/-/g,'') , callback : addViewCount , wiki : page.wiki , file_id : file_id } ) ;
			} ) ;
		} ) ;
	} ) ;
}

function addViewCount ( d ) {
	page_views_stats.done++ ;
	$('#run_progress').width ( (Math.floor(1000*page_views_stats.done/page_views_stats.total)/10) + '%' ) ;

//	console.log ( d.options ) ;
	$.each ( d.data.daily_views , function ( real_day , views ) {
		var day = Date.parse ( real_day ) ;
//		console.log ( d.options ) ;
		if ( page_views_stats.wiki_daily_pageviews[day] === undefined ) page_views_stats.wiki_daily_pageviews[day] = { total : 0 } ;

		if ( page_views_stats.wiki_daily_pageviews[day][d.options.wiki] === undefined ) page_views_stats.wiki_daily_pageviews[day][d.options.wiki] = 0 ;

		page_views_stats.wiki_daily_pageviews[day][d.options.wiki] += views ;
		page_views_stats.wiki_daily_pageviews[day].total += views ;
	} ) ;
//console.log (  page_views_stats.done + " / " + page_views_stats.total ) ;
	page_views_stats.wikipageviews[d.options.wiki] += d.data.monthly_views ;
	page_views_stats.wikipageviews['total'] += d.data.monthly_views ;
	page_views_stats.filepageviews[d.options.file_id] += d.data.monthly_views ;

	$('#wikipageviews_'+sanitizeID(d.options.wiki)).html ( prettyNumber ( page_views_stats.wikipageviews[d.options.wiki] ) ) ;
	$('#wikipageviews_total').html ( prettyNumber ( page_views_stats.wikipageviews['total'] ) ) ;
	$('#filepageviews_'+sanitizeID(d.options.file_id)).html ( prettyNumber ( page_views_stats.filepageviews[d.options.file_id] ) ) ;

	if ( page_views_stats.done == page_views_stats.total ) {
		page_views_stats.finished = true ;
		$('#run_process_container').remove() ;
		showViewPlot() ;
		$("#maintable").trigger("update"); 
		$("#gu_details_table").trigger("update"); 
		$('#resultsTab a:last').tab ('show') ;
	}
}

// helper for returning the weekends in a period
function weekendAreas(axes) {
	var markings = [];
	var d = new Date(axes.xaxis.min);
	// go to the first Saturday
	d.setUTCDate(d.getUTCDate() - ((d.getUTCDay() + 1) % 7))
	d.setUTCSeconds(0);
	d.setUTCMinutes(0);
	d.setUTCHours(0);
	var i = d.getTime();
	do {
		// when we don't set yaxis, the rectangle automatically
		// extends to infinity upwards and downwards
		markings.push({ xaxis: { from: i, to: i + 2 * 24 * 60 * 60 * 1000 } });
		i += 7 * 24 * 60 * 60 * 1000;
	} while (i < axes.xaxis.max);

	return markings;
}

function showTooltip(x, y, contents) {
	$('<div id="tooltip">' + contents + '</div>').css( {
		position: 'absolute',
		display: 'none',
		top: y + 5,
		left: x + 5,
		border: '1px solid #fdd',
		padding: '2px',
		'background-color': '#fee',
		opacity: 0.80
	}).appendTo("body").fadeIn(200);
}

function showViewPlot () {
	var h = '' ;
	h += '<div id="daily_views" class="span12" style="margin-left:0px;height:400px;margin-bottom:20px"></div>' ;
	$('#daily_views_container').html ( h ) ;
	
	var dk = [] ;
	$.each ( page_views_stats.wiki_daily_pageviews , function ( day , data ) { dk.push ( day ) } ) ;
	dk = dk.sort ( function (a,b) { return ( a - b ) } ) ;

	var ln = [] ;
	$.each ( page_views_stats.wikipageviews , function ( wiki , data ) {
		if ( wiki != 'total' ) ln.push ( wiki ) ;
	} ) ;
	ln = ln.sort( function (a,b) { return page_views_stats.wikipageviews[b] - page_views_stats.wikipageviews[a] } ) ;
	ln.unshift ( 'total' ) ;
	while ( ln.length > 6 ) ln.pop() ;
	
	var d = {} ;
	$.each ( ln , function ( dummy , wiki ) { d[wiki] = [] } ) ;
	$.each ( dk , function ( dummy , day ) {
		$.each ( ln , function ( dummy , wiki ) {
			if ( undefined === page_views_stats.wiki_daily_pageviews[day][wiki] ) return ;
			d[wiki].push ( [ day , page_views_stats.wiki_daily_pageviews[day][wiki] ] ) ;
			
		} ) ;
	} ) ;

	
	var options = {
		xaxis: { mode: "time", tickLength: 5 },
		selection: { mode: "x" },
		grid: { markings: weekendAreas , hoverable: true }
	};
	
	var dd = [] ;
	$.each ( ln , function ( dummy , wiki ) {
		dd.push ( { label : wiki , data : d[wiki] } ) ;
	} ) ;
	
	daily_views_plot = $.plot ( 
		$("#daily_views"), 
		dd, 
		options
	);

	var previousPoint = null;
    $("#daily_views").bind("plothover", function (event, pos, item) {
		if (item) {
			if (previousPoint != item.dataIndex) {
				previousPoint = item.dataIndex;
				
				$("#tooltip").remove();
				var x = item.datapoint[0],
					y = item.datapoint[1];
				
				var day = new Date() ;
				day.setTime ( x ) ;
				day = day.toUTCString().replace ( ' 00:00:00 GMT' , '' ) ;
				
				showTooltip(item.pageX, item.pageY,
					item.series.label + " on " + day + " : " + prettyNumber ( y ) + " views" ) ;
//							item.series.label + " of " + x + " = " + y);
			}
		}
		else {
			$("#tooltip").remove();
			previousPoint = null;            
		}
    });
	
//	$($("#daily_views .legend div").get(0)).css ( { 'max-height' : '300px' , overflow : 'auto' } ) ;
}

$(document).ready ( function () {
	loadMenuBarAndContent ( { toolname : 'GLAMorous 2' , meta : 'GLAMorous 2' , content : 'form.html' , run : function () {
		wikiDataCache.ensureSiteInfo ( [ { lang:'commons' , project:'wikimedia' } ] , function () {
			httpsAlert ( 'result_container' , true ) ;
			ts2Interface.addInputPageExistsVerification ( { input:'#category' , fieldset:'#fs_cat' , lang:'commons' , project:'wikimedia' , ns:14 ,
				yes : updateCatTreeStats ,
				no : updateCatTreeStats
			} ) ;
			ts2Interface.addInputPageExistsVerification ( { input:'#user' , fieldset:'#fs_user' , lang:'commons' , project:'wikimedia' , ns:2 } ) ;
			ts2Interface.addInputPageExistsVerification ( { input:'#page' , fieldset:'#fs_page' , lang:'commons' , project:'wikimedia' , ns:0 } ) ;
			
			$.each ( [ 'category' , 'user' , 'page', 'pagepile' ] , function ( k , v ) {
				$('#tr_'+v).click ( function () { $("input[name='mode']")[k].checked = true ; } ) ;
				$('#'+v).change ( function () { $("input[name='mode']")[k].checked = true ; } ) ;
			} ) ;
			$('#depth').change ( function () { $("input[name='mode']")[0].checked = true ; } ) ;
			$('#theform table td').css ( { 'vertical-align' : 'top' } ) ;
			$('#depth').change ( function () { updateCatTreeStats() ; } ) ;
			$('#category').focus() ;
			
			initializeFromParameters () ;
		} ) ;
	} } ) ;
} ) ;
