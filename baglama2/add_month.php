#!/usr/bin/php
<?PHP

# cd /data/project/glamtools/baglama2
# jsub -mem 8g -cwd ./add_month.php YEAR MONTH

# DB is s51203__baglama2_p

error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);
ini_set('display_errors', 'On');

ini_set('memory_limit','7000M');
//set_time_limit ( 60 * 60 * 10 ) ;


if ( !isset ( $argv[2] ) ) {
	print "Usage: add_month.php YEAR MONTH\n" ;
	print "Adds all pages for the groups in that month, where not exists\n" ;
	exit ( 1 ) ;
}
$year = $argv[1] * 1 ;
$month = $argv[2] * 1 ;
$main_group = -1 ;
if ( isset($argv[3]) ) {
	if ( preg_match ( '/^\d+$/' , $argv[3] ) ) {
		$main_group = $argv[3] ;
		$mode = '' ;
	} else $mode = $argv[3] ;
} else $mode = '' ;

include_once ( '../public_html/php/common.php' ) ;
$commons = openDB ( 'commons' , 'wikimedia' ) ;
$db = openToolDB ( 'baglama2_p' ) ;

$sites = array() ;

// ____________________________________________________________________________________________________________

// Ensure all globalimagelinks sites exist in site table
function ensureSites () {
	global $commons , $sites ;
	
	$db = openToolDB ( 'baglama2_p' ) ;
	$existing = array() ;
	$sql = "SELECT * FROM sites" ;
	if(!$result = $db->query($sql)) die('There was an error running the query 1 [' . $db->error . ']');
	while($o = $result->fetch_object()){
		$existing[$o->giu_code] = $o ;
	}
	
	$sql = "select distinct gil_wiki from globalimagelinks" ;
	if(!$result = $commons->query($sql)) die('There was an error running the query 2 [' . $commons->error . ']');
	while($o = $result->fetch_object()){
		if ( isset ( $existing[$o->gil_wiki] ) ) continue ;
		if ( $o->gil_wiki == '' ) continue ; // Blank wiki; WTF?
		
		
		preg_match ( '/^(.+)(wik.+)$/' , $o->gil_wiki , $parts ) ;
		if ( count ( $parts ) != 3 ) {
			print "Unusual wiki \"" . $o->gil_wiki . "\", aborting :\n" ;
			print_r ( $parts ) ;
			print_r ( $o ) ;
			exit ( 0 ) ;
		}
		
		$giu_code = $o->gil_wiki ;
		$language = str_replace ( '_' , '-' , $parts[1] ) ;
		$project = $parts[2] ;
		if ( $language == 'commons' ) $project = 'wikimedia' ;
		if ( substr ( $language , 0 , 9 ) == 'wikimania' ) $project = 'wikimedia' ;
		if ( $project == 'wiki' ) $project = 'wikipedia' ;
		$server = "$language.$project.org" ;
		
		$sql = "INSERT INTO sites (grok_code,server,giu_code,project,language) VALUES ('','$server','$giu_code','$project','$language')" ;
		if(!$r2 = $db->query($sql)) die('There was an error running the query 3 [' . $db->error . ']');
	}

	// As above, so below
	$sql = "SELECT * FROM sites" ;
	if(!$result = $db->query($sql)) die('There was an error running the query 4 [' . $db->error . ']');
	while($o = $result->fetch_object()){
		$sites[$o->giu_code] = $o ;
	}
}

function addPages ( $g ) {
	global $commons , $sites , $month , $year , $db ;
	
	$g->category = utf8_encode($g->category) ;
	
	print "$year/$month : " . $g->category . "\n" ;

	$db = openToolDB ( 'baglama2_p' ) ;
	
	$cats = array() ;
	findSubcats ( $commons , array($g->category) , $cats , $g->depth ) ;
	

	$group_status_id = '' ;
	if ( !isset ( $g->group_status_id ) ) {
		$sql = "INSERT INTO group_status (group_id,year,month,status) VALUES (".$g->id.",$year,$month,'GENERATING PAGE LIST') on duplicate key update status=values(status)" ;
		if(!$r2 = $db->query($sql)) die('There was an error running the query 5 [' . $db->error . ']');
		$group_status_id = $db->insert_id ;
	} else {
		$group_status_id = $g->group_status_id ;
	}

	$month_padded = $month ;
	if ( strlen($month) == 1 ) $month_padded = "0$month" ;
	
	$sql = "SELECT globalimagelinks.* FROM globalimagelinks,page,categorylinks WHERE gil_to=page_title AND cl_from=page_id AND page_namespace=6 AND cl_to IN ('" . implode("','",$cats) . "') AND cl_timestamp < '" . $year . "-" . $month_padded . "-31'" ; //  AND page_is_redirect=0

	$commons->close() ;
	$commons = openDB ( 'commons' , 'wikimedia' ) ;
	
	$commands = array() ;
	
	if(!$result = $commons->query($sql)) die('There was an error running the query 6 [' . $commons->error . ']');
	while($o = $result->fetch_object()){
		if ( !isset($sites[$o->gil_wiki]) ) {
			print "Unknown site " . $o->gil_wiki . ", skipping.\n" ;
			continue ;
		}
		
		$title = $o->gil_page_title ;
		
		$sql = "INSERT IGNORE INTO views (site,page_id,title,month,year,done,namespace_id) VALUES (" ;
		$sql .= $sites[$o->gil_wiki]->id . "," ;
		$sql .= $o->gil_page . "," ;
		$sql .= "'" . $db->real_escape_string($title) . "'," ;
		$sql .= "$month,$year,0," ;
		$sql .= $o->gil_page_namespace_id . ")" ;
		
		$c = array ( $sql , $o ) ;
		$commands[] = json_encode ( $c ) ;
	}
	
	
	$db->close() ;
	$db = openToolDB ( 'baglama2_p' ) ;

//	$db->autocommit(false) ;
	$cnt = 0 ;
	
	foreach ( $commands AS $d ) {
		$d = json_decode($d) ;
		$o = $d[1] ;
		if(!$r2 = $db->query($d[0])) die('There was an error running the query 7 [' . $db->error . ']');
		$last_id = $db->insert_id ;
		
		if ( $last_id == 0 ) {
			$sql = "INSERT IGNORE INTO group2view (group_status_id,view_id,image) SELECT " ;
			$sql .= $group_status_id . ",views.id,'" . $db->real_escape_string($o->gil_to) ;
			$sql .= "' FROM views WHERE" ;
			$sql .= " views.site=" . $sites[$o->gil_wiki]->id . " AND " ;
			$sql .= " views.page_id=" . $o->gil_page . " AND " ;
			$sql .= " views.month=$month AND views.year=$year LIMIT 1" ;
		} else {
			$sql = "INSERT IGNORE INTO group2view (group_status_id,view_id,image) VALUES (" ;
			$sql .= $group_status_id . "," ;
			$sql .= $last_id . "," ;
			$sql .= "'" . $db->real_escape_string($o->gil_to) . "')" ;
		}
		if(!$r2 = $db->query($sql)) die('There was an error running the query 8 [' . $db->error . ']');
	
	
		$cnt++ ;
//		if ( $cnt % 10000 == 0 ) $db->commit() ;
	}

//	$db->autocommit(true) ;
	
	$sql = "INSERT INTO group_status (group_id,year,month,status) VALUES (".$g->id.",$year,$month,'PAGE LIST GENERATED') on duplicate key update status=values(status)" ;
	if(!$r2 = $db->query($sql)) die('There was an error running the query 9 [' . $db->error . ']');
}


// ____________________________________________________________________________________________________________


ensureSites() ;


if ( $mode == 'force' ) {
	while ( 1 ) {
//		$db->close() ;
		$db = openToolDB ( 'baglama2_p' ) ;

		$sql = "SELECT group_status.group_id AS id,group_status.id AS group_status_id,category,depth FROM groups,group_status WHERE group_status.group_id=groups.id AND group_status.month=$month AND group_status.year=$year AND status='GENERATING PAGE LIST' order by rand() limit 1" ;
		if(!$result = $db->query($sql)) die('There was an error running the query 10 [' . $db->error . ']');
		if($o = $result->fetch_object()){
	//		print_r ( $o ) ;
			addPages ( $o ) ;
		} else {
			break ;
		}
	}
//	exit ( 0 ) ;
}

while ( 1 ) {
//	$db->close() ;
	$db = openToolDB ( 'baglama2_p' ) ;
	$sql = "SELECT * FROM groups WHERE NOT EXISTS (SELECT * FROM group_status WHERE group_status.group_id=groups.id AND group_status.month=$month AND group_status.year=$year LIMIT 1)" ;
	if ( $main_group >= 0 ) $sql .= " AND groups.id=$main_group" ;
	$sql .= "  ORDER BY rand() LIMIT 1" ;
	
	if(!$result = $db->query($sql)) die('There was an error running the query 11 [' . $db->error . ']');
	$db->close() ;
	if($o = $result->fetch_object()){
		addPages ( $o ) ;
	} else {
		break ;
	}
}

print "COMPLETION\n" ;

?>